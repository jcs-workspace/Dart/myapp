@echo off
:: ========================================================================
:: $File: run.bat $
:: $Date: 2023-08-08 23:32:12 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

cd ../

flutter clean && flutter run -d windows
