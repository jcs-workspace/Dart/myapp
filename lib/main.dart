import 'package:flutter/material.dart';

// Something else
//
// okay

List<int> numbers = [1, 5, 10, 44, 11, 33];

/// something
void onClick() {
  print('click');
}

void main() {
  //runApp(const MyApp());
  runApp(MaterialApp(
    home: Home(),
  ));
}

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('my first app'),
        centerTitle: true,
        backgroundColor: Colors.red[600],
      ),
      body: Center(
        //child: Image.asset('assets/gnu.png'),
        //child: Icon(Icons.airport_shuttle, color: Colors.lightBlue, size: 50.0),
        child: ElevatedButton(
          onPressed: () {
            print('hello');
          },
          style: ButtonStyle(
              backgroundColor:
                  MaterialStateProperty.all<Color>(Colors.lightBlue)),
          child: Text('click me'),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: onClick,
        backgroundColor: Colors.red[600],
        child: const Text('click'),
      ),
    );
  }
}
